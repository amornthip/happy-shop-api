defmodule HappyShopApi.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :string
      add :category, :string
      add :price, :integer
      add :sale_price, :integer
      add :sale_text, :string
      add :sold_out, :boolean, default: false, null: false
      add :under_sale, :boolean, default: false, null: false

      timestamps()
    end

  end
end
