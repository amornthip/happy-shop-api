# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     HappyShopApi.Repo.insert!(%HappyShopApi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias HappyShopApi.Shops.Product

HappyShopApi.Repo.insert!(%Product{
  name: "FENTY BEAUTY by Rihanna",
  category: "Makeup",
  price: 1000,
  sale_price: 500,
  sale_text: "50% OFF",
  sold_out: false,
  under_sale: false
})

HappyShopApi.Repo.insert!(%Product{
  name: "Sugar Lip Treatment",
  category: "Makeup",
  price: 1000,
  sale_price: 500,
  sale_text: "50% OFF",
  sold_out: false,
  under_sale: false
})

HappyShopApi.Repo.insert!(%Product{
  name: "Matte Eyeshadow Palette",
  category: "Makeup",
  price: 1100,
  sale_price: 550,
  sale_text: "50% OFF",
  sold_out: false,
  under_sale: false
})

HappyShopApi.Repo.insert!(%Product{
  name: "Cream Lip Stain",
  category: "Makeup",
  price: 350,
  sale_price: 175,
  sale_text: "50% OFF",
  sold_out: false,
  under_sale: false
})

HappyShopApi.Repo.insert!(%Product{
  name: "Everlasting Liquid Lipstick",
  category: "Makeup",
  price: 900,
  sale_price: 450,
  sale_text: "50% OFF",
  sold_out: false,
  under_sale: false
})

HappyShopApi.Repo.insert!(%Product{
  name: "Match Stix Trios",
  category: "Makeup",
  price: 2100,
  sale_price: 1050,
  sale_text: "50% OFF",
  sold_out: false,
  under_sale: false
})
