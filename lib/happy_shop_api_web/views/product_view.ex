defmodule HappyShopApiWeb.ProductView do
  use HappyShopApiWeb, :view
  alias HappyShopApiWeb.ProductView

  def render("index.json", %{products: products}) do
    %{data: render_many(products, ProductView, "product.json")}
  end

  def render("show.json", %{product: product}) do
    %{data: render_one(product, ProductView, "product.json")}
  end

  def render("product.json", %{product: product}) do
    %{id: product.id,
      name: product.name,
      category: product.category,
      price: product.price,
      sale_price: product.sale_price,
      sale_text: product.sale_text,
      sold_out: product.sold_out,
      under_sale: product.under_sale}
  end
end
