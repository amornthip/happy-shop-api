defmodule HappyShopApiWeb.Router do
  use HappyShopApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", HappyShopApiWeb do
    pipe_through :api
    resources "/products", ProductController, only: [:index, :show]
  end
end
