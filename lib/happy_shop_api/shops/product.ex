defmodule HappyShopApi.Shops.Product do
  use Ecto.Schema
  import Ecto.Changeset


  schema "products" do
    field :category, :string
    field :name, :string
    field :price, :integer
    field :sale_price, :integer
    field :sale_text, :string
    field :sold_out, :boolean, default: false
    field :under_sale, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :category, :price, :sale_price, :sale_text, :sold_out, :under_sale])
    |> validate_required([:name, :category, :price, :sale_price, :sale_text, :sold_out, :under_sale])
  end
end
