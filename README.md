# HappyShopApi

## Setup

You can use docker to install and setup this application.
To get this up and running, follow these commands:

```
$ docker-compose build
```

```
$ docker-compose up
```

The backend service for happy shop will run on port 4000.

## Running Tests

```
$ docker-compose exec web mix test --color
```