use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :happy_shop_api, HappyShopApiWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :happy_shop_api, HappyShopApi.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "happy_shop_api_test",
  hostname: "db",
  pool: Ecto.Adapters.SQL.Sandbox
