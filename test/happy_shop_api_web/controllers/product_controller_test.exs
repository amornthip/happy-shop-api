defmodule HappyShopApiWeb.ProductControllerTest do
  use HappyShopApiWeb.ConnCase

  alias HappyShopApi.Shops
  alias HappyShopApi.Shops.Product

  @create_attrs %{category: "some category", name: "some name", price: 42, sale_price: 42, sale_text: "some sale_text", sold_out: true, under_sale: true}
  @update_attrs %{category: "some updated category", name: "some updated name", price: 43, sale_price: 43, sale_text: "some updated sale_text", sold_out: false, under_sale: false}
  @invalid_attrs %{category: nil, name: nil, price: nil, sale_price: nil, sale_text: nil, sold_out: nil, under_sale: nil}

  def fixture(:product) do
    {:ok, product} = Shops.create_product(@create_attrs)
    product
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all products", %{conn: conn} do
      conn = get conn, product_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create product" do
    test "renders product when data is valid", %{conn: conn} do
      conn = post conn, product_path(conn, :create), product: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, product_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "category" => "some category",
        "name" => "some name",
        "price" => 42,
        "sale_price" => 42,
        "sale_text" => "some sale_text",
        "sold_out" => true,
        "under_sale" => true}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, product_path(conn, :create), product: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update product" do
    setup [:create_product]

    test "renders product when data is valid", %{conn: conn, product: %Product{id: id} = product} do
      conn = put conn, product_path(conn, :update, product), product: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, product_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "category" => "some updated category",
        "name" => "some updated name",
        "price" => 43,
        "sale_price" => 43,
        "sale_text" => "some updated sale_text",
        "sold_out" => false,
        "under_sale" => false}
    end

    test "renders errors when data is invalid", %{conn: conn, product: product} do
      conn = put conn, product_path(conn, :update, product), product: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete product" do
    setup [:create_product]

    test "deletes chosen product", %{conn: conn, product: product} do
      conn = delete conn, product_path(conn, :delete, product)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, product_path(conn, :show, product)
      end
    end
  end

  defp create_product(_) do
    product = fixture(:product)
    {:ok, product: product}
  end
end
