defmodule HappyShopApi.ShopsTest do
  use HappyShopApi.DataCase

  alias HappyShopApi.Shops

  describe "products" do
    alias HappyShopApi.Shops.Product

    @valid_attrs %{category: "some category", name: "some name", price: 42, sale_price: 42, sale_text: "some sale_text", sold_out: true, under_sale: true}
    @update_attrs %{category: "some updated category", name: "some updated name", price: 43, sale_price: 43, sale_text: "some updated sale_text", sold_out: false, under_sale: false}
    @invalid_attrs %{category: nil, name: nil, price: nil, sale_price: nil, sale_text: nil, sold_out: nil, under_sale: nil}

    def product_fixture(attrs \\ %{}) do
      {:ok, product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Shops.create_product()

      product
    end

    test "list_products/0 returns all products" do
      product = product_fixture()
      assert Shops.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = product_fixture()
      assert Shops.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      assert {:ok, %Product{} = product} = Shops.create_product(@valid_attrs)
      assert product.category == "some category"
      assert product.name == "some name"
      assert product.price == 42
      assert product.sale_price == 42
      assert product.sale_text == "some sale_text"
      assert product.sold_out == true
      assert product.under_sale == true
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Shops.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = product_fixture()
      assert {:ok, product} = Shops.update_product(product, @update_attrs)
      assert %Product{} = product
      assert product.category == "some updated category"
      assert product.name == "some updated name"
      assert product.price == 43
      assert product.sale_price == 43
      assert product.sale_text == "some updated sale_text"
      assert product.sold_out == false
      assert product.under_sale == false
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = product_fixture()
      assert {:error, %Ecto.Changeset{}} = Shops.update_product(product, @invalid_attrs)
      assert product == Shops.get_product!(product.id)
    end

    test "delete_product/1 deletes the product" do
      product = product_fixture()
      assert {:ok, %Product{}} = Shops.delete_product(product)
      assert_raise Ecto.NoResultsError, fn -> Shops.get_product!(product.id) end
    end

    test "change_product/1 returns a product changeset" do
      product = product_fixture()
      assert %Ecto.Changeset{} = Shops.change_product(product)
    end
  end
end
